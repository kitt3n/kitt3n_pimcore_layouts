<?php

namespace KITT3N\Pimcore\LayoutsBundle\Controller;

use Pimcore\Config;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class DefaultController extends FrontendController
{
    public function indexAction(Request $request)
    {
        $aReturn = $this->getBamParams($request);

        switch (true) {
            case ! $aReturn['bIsAllowed']:
                header('HTTP/1.0 403 Forbidden');
                echo 'Forbidden!';
                die;
                break;
        }

        $this->view->oDocument = $aReturn["oDocument"];
        $this->view->aDocumentProperties = $aReturn["aDocumentProperties"];
        $this->view->oUser = $aReturn["oUser"];
        $this->view->oWebsiteConfig = $aReturn["oWebsiteConfig"];
        $this->view->bIsAllowed = $aReturn["bIsAllowed"];

        $this->view->aBamParams = $aReturn;
    }

    public function getBamParams(Request $request)
    {
        /* @var \Pimcore\Model\Document\Page $oDocument */
        $oDocument = $this->document;
        /* @var array|null $xDocumentProperties */
        $xDocumentProperties = $oDocument->getProperties();
        /* @var array $aDocumentProperties */
        $aDocumentProperties = $xDocumentProperties === null ? [] : $xDocumentProperties;
        /** @var \Pimcore\Model\DataObject\User $oUser */
        $oUser = $this->getUser();

        $sLanguage = $oDocument->getProperty("language");
        $sLocale = $request->getLocale();
        if ( ! null == $sLocale) {
            $request->setLocale($sLocale);
        }

        /* @var \Pimcore\Config\Config $aWebsiteConfig */
        $oWebsiteConfig = Config::getWebsiteConfig($sLocale);

        $aRestrictions = [
            "aAssetRestrictions" => [],
            "bDocumentRestriction" => true,
            "oUser" => $oUser,
            "oGroup" => null,
        ];

        /* @var bool $bIsAllowed */
        $bIsAllowed = true;

        $bEditmode = $this->editmode;

        if (! $bEditmode) {

            if ($oUser instanceof \KITT3N\Pimcore\RestrictionsBundle\Model\DataObject\User) {

                /** @var \Pimcore\Model\DataObject\Group $oGroup */
                $oGroup = $oUser->getGroup();
                $aRestrictions["oGroup"] = $oGroup;

                // Check if current document is blacklisted
                $aBlacklistedDocuments = $oUser->getGroup()->getDocumentRestrictions();
                if (! empty($aBlacklistedDocuments)) {
                    $iCurrentDocumentId = $oDocument->getId();
                    foreach ($aBlacklistedDocuments as $oBlacklistedDocument) {
                        if ($oBlacklistedDocument->getId() === $iCurrentDocumentId) {
                            $bIsAllowed = false;
                            $aRestrictions["bDocumentRestriction"] = $bIsAllowed;
                            break;
                        }
                    }
                }

                /* @var array|null $xRestrictions */
                $xRestrictions = $oUser->getGroup()->getAssetRestrictions();
                $aRestrictions["aAssetRestrictions"] = $xRestrictions === null ? [] : $xRestrictions;

            } else {

                /* @var \Pimcore\Model\DataObject\Group $oBamGuestGroup */
                $oBamGuestGroup = $oWebsiteConfig->get('bam.restrictions.group.guest');

                if ($oBamGuestGroup === null or ! ($oBamGuestGroup instanceof \Pimcore\Model\DataObject\Group)) {
                    // Error in configuration
                } else {

                    $aRestrictions["oGroup"] = $oBamGuestGroup;

                    // Check if current document is blacklisted
                    $aBlacklistedDocuments = $oBamGuestGroup->getDocumentRestrictions();
                    if (! empty($aBlacklistedDocuments)) {
                        $iCurrentDocumentId = $oDocument->getId();
                        foreach ($aBlacklistedDocuments as $oBlacklistedDocument) {
                            if ($oBlacklistedDocument->getId() === $iCurrentDocumentId) {
                                $bIsAllowed = false;
                                $aRestrictions["bDocumentRestriction"] = $bIsAllowed;
                                break;
                            }
                        }
                    }

                    $xRestrictions = $oBamGuestGroup->getAssetRestrictions();
                    $aRestrictions["aAssetRestrictions"] = $xRestrictions === null ? [] : $xRestrictions;

                }

            }

        }

        return [
            "oDocument" => $oDocument,
            "aDocumentProperties" => $aDocumentProperties,
            "oUser" => $oUser,
            "oWebsiteConfig" => $oWebsiteConfig,
            "bIsAllowed" => $bIsAllowed,
            "aRestrictions" => $aRestrictions,
            "sLocale" => $sLocale,
            "sLanguage" => $sLanguage,
        ];

    }
}
