<?php

namespace KITT3N\Pimcore\LayoutsBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;
use PackageVersions\Versions;

class Kitt3nPimcoreLayoutsBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/kitt3npimcorelayouts/js/pimcore/startup.js'
        ];
    }

    /**
     * @inheritDoc
     */
    public function getVersion()
    {
        return Versions::getVersion('kitt3n/pimcore-layouts');
    }

}
