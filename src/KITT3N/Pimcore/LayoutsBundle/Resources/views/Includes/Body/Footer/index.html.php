<?php
/**
 * @var array $aBamParams
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config         $aBamParams ["oWebsiteConfig"]
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config       $aBamParams ["oWebsiteConfig"]
 */
?>

<footer>

    <div class="grid-container c4-c8">
        <div class="column">
            &copy; <?= date("Y");?>&nbsp;<a target="_blank" href="https://www.zwei14.de">ZWEI14</a>
        </div>
        <div class="column">
            <nav>

                <?php
                /* @var \Pimcore\Model\DataObject\Group $oBamFooterImprint */
                $oBamFooterImprint = $aBamParams["oWebsiteConfig"]->get('bam.footer.imprint');
                if ($oBamFooterImprint === null or ! ($oBamFooterImprint instanceof \Pimcore\Model\Document\Page)): ?>

                    <a style="color: red;background: url(/bundles/kitt3npimcorelayouts/icons/logo_danger.png) no-repeat center center;background-size: 18px;"
                       href="javascript:;">
                        <?= $this->translate("LayoutsBundle__footer_a_imprint"); ?>
                    </a>

                <?php else: ?>

                    <a href="<?= $oBamFooterImprint->getHref(); ?>"><?= $this->translate("LayoutsBundle__footer_a_imprint"); ?></a>

                <?php endif; ?>

                <?php
                /* @var \Pimcore\Model\DataObject\Group $oBamFooterPrivacy */
                $oBamFooterPrivacy = $aBamParams["oWebsiteConfig"]->get('bam.footer.privacy');
                if ($oBamFooterPrivacy === null or ! ($oBamFooterPrivacy instanceof \Pimcore\Model\Document\Page)): ?>

                    <a style="color: red;background: url(/bundles/kitt3npimcorelayouts/icons/logo_danger.png) no-repeat center center;background-size: 18px;"
                       href="javascript:;">
                        <?= $this->translate("LayoutsBundle__footer_a_privacy"); ?>
                    </a>

                <?php else: ?>

                    <a href="<?= $oBamFooterPrivacy->getHref(); ?>"><?= $this->translate("LayoutsBundle__footer_a_privacy"); ?></a>

                <?php endif; ?>

                <?php
                /* @var \Pimcore\Model\DataObject\Group $oBamFooterPrivacy */
                $oBamFooterContact = $aBamParams["oWebsiteConfig"]->get('bam.footer.contact');
                if ($oBamFooterContact === null or ! ($oBamFooterContact instanceof \Pimcore\Model\Document\Page)): ?>

                    <a style="color: red;background: url(/bundles/kitt3npimcorelayouts/icons/logo_danger.png) no-repeat center center;background-size: 18px;"
                       href="javascript:;">
                        <?= $this->translate("LayoutsBundle__footer_a_contact"); ?>
                    </a>

                <?php else: ?>

                    <a href="<?= $oBamFooterContact->getHref(); ?>"><?= $this->translate("LayoutsBundle__footer_a_contact"); ?></a>

                <?php endif; ?>

            </nav>
        </div>
    </div>

</footer>