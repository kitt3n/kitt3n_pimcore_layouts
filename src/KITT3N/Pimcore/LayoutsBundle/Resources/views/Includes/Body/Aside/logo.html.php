<?php
/* @var \Pimcore\Model\DataObject\Group $oBamCustomLogo */
$oBamCustomLogo = $oWebsiteConfig->get('bam.custom.logo');
if ($oBamCustomLogo === null or ! ($oBamCustomLogo instanceof \Pimcore\Model\Asset\Image)): ?>

    <div id="logo" class="portrait" alt="bam.custom.logo missing!" title="bam.custom.logo missing!"
         style="background: url(/bundles/kitt3npimcorelayouts/icons/logo_danger.png) no-repeat;" >
    </div>

<?php else: ?>

    <?php if ($oBamCustomLogo->getWidth() > $oBamCustomLogo->getHeight()): ?>

        <div id="logo" class="landscape"
             style="background: url(<?= $oBamCustomLogo->getThumbnail(["width" => 140, "interlace" => true, "quality" => 100]) ?>) no-repeat;" >
        </div>

    <?php else: ?>

        <div id="logo" class="portrait"
            style="background: url(<?= $oBamCustomLogo->getThumbnail(["height" => 110, "interlace" => true, "quality" => 100]) ?>) no-repeat;" >
        </div>

    <?php endif; ?>



<?php endif; ?>

