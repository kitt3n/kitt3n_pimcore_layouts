<?php
/**
 * @var array $aBamParams
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config         $aBamParams ["oWebsiteConfig"]
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config       $aBamParams ["oWebsiteConfig"]
 */
?>

<div class="user navigation aside">

    <ul class="navigation">

        <?php if ($aBamParams ["oUser"] !== null): ?>

            <li>
                <a href="<?= $this->path('restrictions_logout') ?>">
                    <?= $this->translate("LayoutsBundle__aside_logout"); ?>
                </a>
            </li>

        <?php else: ?>

            <li>
                <a href="<?= $this->path('restrictions_login') ?>">
                    <?= $this->translate("LayoutsBundle__aside_login"); ?>
                </a>
            </li>

        <?php endif; ?>

    </ul>

    <?php if ($aBamParams ["oUser"] !== null): ?>
        <ul class="navigation current">
            <li>
                <?= $this->translate("LayoutsBundle__aside_active"); ?>:<br>
                <strong><?= $aBamParams ["oUser"]->getUsername(); ?></strong>
            </li>
        </ul>
    <?php endif; ?>

</div>
