<?php

/**
 * @var array $aBamParams
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config         $aBamParams ["oWebsiteConfig"]
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config       $aBamParams ["oWebsiteConfig"]
 */

/* @var \Pimcore\Model\Document\Page $oBamGuidelinesRoot */
$oBamRootDocument = $aBamParams["oWebsiteConfig"]->get('bam.root.document');

// get root node if there is no document defined (for pages which are routed directly through static route)
if(!$aBamParams["oDocument"] instanceof \Pimcore\Model\Document\Page) {
    $aBamParams["oDocument"] = \Pimcore\Model\Document\Page::getById(1);
}

// get the document which should be used to start in navigation | default home
if( ! $oBamRootDocument instanceof \Pimcore\Model\Document\Page and ! $oBamRootDocument instanceof \Pimcore\Model\Document\Link) {
$oBamRootDocument = \Pimcore\Model\Document\Page::getById(1);
}

// this returns us the navigation container we can use to render the navigation
/* @var \Pimcore\Navigation\Container $mainNavigation */
$mainNavigation = $this->navigation()->buildNavigation($aBamParams["oDocument"], $oBamRootDocument, null, function($page, $document) {

    /* @var \Pimcore\Navigation\Page\Document $page*/
    /* @var \Pimcore\Model\Document\Page $document*/
    $oUser = $this->oUser;
    $oWebsiteConfig = $this->oWebsiteConfig;
    $bIsAllowed = true;

    if ($oUser instanceof \KITT3N\Pimcore\RestrictionsBundle\Model\DataObject\User) {

        // Check if current document is blacklisted
        $aBlacklistedDocuments = $oUser->getGroup()->getDocumentRestrictions();
        if (! empty($aBlacklistedDocuments)) {
            $iCurrentDocumentId = intval($document->getId());
            foreach ($aBlacklistedDocuments as $oBlacklistedDocument) {
                $iBlacklistedDocumentId = intval($oBlacklistedDocument->getId());
                if ($iBlacklistedDocumentId === $iCurrentDocumentId) {
                    $bIsAllowed = false;
                    break;
                }
            }
        }

    } else {

        /* @var \Pimcore\Model\DataObject\Group $oBamGuestGroup */
        $oBamGuestGroup = $oWebsiteConfig->get('bam.restrictions.group.guest');

        if ($oBamGuestGroup === null or ! ($oBamGuestGroup instanceof \Pimcore\Model\DataObject\Group)) {
            // Error in configuration
            $x = 1;
        } else {
            // Check if current document is blacklisted
            $aBlacklistedDocuments = $oBamGuestGroup->getDocumentRestrictions();
            if (! empty($aBlacklistedDocuments)) {
                $iCurrentDocumentId = intval($document->getId());
                foreach ($aBlacklistedDocuments as $oBlacklistedDocument) {
                    $iBlacklistedDocumentId = intval($oBlacklistedDocument->getId());
                    if ($iBlacklistedDocumentId === $iCurrentDocumentId) {
                        $bIsAllowed = false;
                        break;
                    }
                }
            }
        }

    }

    if ( ! $bIsAllowed) {
        $page->setVisible(false);
    }

}, "nav_aside_" . ($this->oUser !== null ? $this->oUser->getGroup()->getId() : "")); //false

// later you can render the navigation
echo "<div class=\"navigation aside\">" . $this->navigation()->render($mainNavigation) . "</div>";