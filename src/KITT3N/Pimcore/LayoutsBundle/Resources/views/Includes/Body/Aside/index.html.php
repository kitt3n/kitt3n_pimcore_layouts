<?php
/**
 * @var array $aBamParams
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config         $aBamParams ["oWebsiteConfig"]
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config       $aBamParams ["oWebsiteConfig"]
 */
?>
<input type="checkbox" id="openAside" name="open" />
<aside>
    <label class="openAside" for="openAside"></label>

    <?= $this->template(
        "@Kitt3nPimcoreLayoutsBundle/Resources/views/Includes/Body/Aside/logo.html.php",
        [
            "oWebsiteConfig" => $oWebsiteConfig,
            "oDocument" => $oDocument,
            "oUser" => $oUser,
            "aBamParams" => $aBamParams,
        ]
    ); ?>

    <?= $this->template(
        "@Kitt3nPimcoreLayoutsBundle/Resources/views/Includes/Body/Aside/navigation.html.php",
        [
            "oWebsiteConfig" => $oWebsiteConfig,
            "oDocument" => $oDocument,
            "oUser" => $oUser,
            "aBamParams" => $aBamParams,
        ]
    ); ?>

    <?= $this->template(
        "@Kitt3nPimcoreLayoutsBundle/Resources/views/Includes/Body/Aside/user.html.php",
        [
            "oWebsiteConfig" => $oWebsiteConfig,
            "oDocument" => $oDocument,
            "oUser" => $oUser,
            "aBamParams" => $aBamParams,
        ]
    ); ?>

</aside>