<?php

/**
 * @var array $aBamParams
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config         $aBamParams ["oWebsiteConfig"]
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config       $aBamParams ["oWebsiteConfig"]
 */

?>

<header id="header" <?php if($this->editmode): ?> class="header edit"<?php endif; ?>>

    <div class="grid-container columns c12">
        <div class="column">
            <h1>
                <?= $aBamParams["oDocument"]->getTitle(); ?>
            </h1>
        </div>

    </div>

    <?= $this->template(
        "@Kitt3nPimcoreLayoutsBundle/Resources/views/Includes/Body/Header/navigation.anchor.html.php",
        [
            "oDocument" => $aBamParams["oDocument"],
            "aBamParams" => $aBamParams,
        ]
    ); ?>

</header>
