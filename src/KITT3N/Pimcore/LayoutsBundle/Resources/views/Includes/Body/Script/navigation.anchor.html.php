<script>

    var aAnchorWrappers = document.querySelectorAll(".element--anchor");
    //var aAnchors = document.querySelectorAll("a.anchor");
    var aAnchorLinks = document.querySelectorAll(".anchorLink");
    var aAnchorStates = [];

    function scrollspy () {

        var iOffset = (document.getElementById("header").offsetHeight) - 0 + 10;

        var windowScrollY = window.pageYOffset;

        for (var i = 0; i < aAnchorWrappers.length; i++) {

            switch (true) {

                case windowScrollY >= (aAnchorWrappers[i].offsetTop - 0 - iOffset):

                    if (i > 0) {
                        aAnchorStates[i-1] = false;
                    }
                    aAnchorStates[i] = true;

                    break;
                default:

                    if (i === 0) {
                        aAnchorStates[i] = true;
                    } else {
                        aAnchorStates[i] = false;
                    }

            }

        }

        for (var j = 0; j < aAnchorStates.length; j++) {

            switch (true) {

                case true === aAnchorStates[j]:

                    aAnchorLinks[j].setAttribute('class', 'anchorLink active');

                    break;
                default:

                    aAnchorLinks[j].setAttribute('class', 'anchorLink');

            }

        }

    }

    scrollspy();

    window.addEventListener('scroll', debounce(scrollspy));

</script>