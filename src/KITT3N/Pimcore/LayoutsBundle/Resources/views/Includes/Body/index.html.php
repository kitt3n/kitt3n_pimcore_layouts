<?php

/**
 * @var array $aBamParams
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config         $aBamParams ["oWebsiteConfig"]
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config       $aBamParams ["oWebsiteConfig"]
 */

?>

<body class="index">
    <div class="container">

        <?= $this->template(
            "@Kitt3nPimcoreLayoutsBundle/Resources/views/Includes/Body/Aside/index.html.php",
            [
                "oWebsiteConfig" => $oWebsiteConfig,
                "oDocument" => $oDocument,
                "oUser" => $oUser,
                "aRestrictions" => $aRestrictions,
                "aBamParams" => $aBamParams,
            ]
        ); ?>

        <?= $this->template(
            "@Kitt3nPimcoreLayoutsBundle/Resources/views/Includes/Body/Header/index.html.php",
            [
                "oWebsiteConfig" => $oWebsiteConfig,
                "oDocument" => $oDocument,
                "oUser" => $oUser,
                "aRestrictions" => $aRestrictions,
                "aBamParams" => $aBamParams,
            ]
        ); ?>

        <main>

            <?php
            $this->slots()->output('_content')
            ?>

        </main>

        <?= $this->template(
            "@Kitt3nPimcoreLayoutsBundle/Resources/views/Includes/Body/Footer/index.html.php",
            [
                "oWebsiteConfig" => $oWebsiteConfig,
                "oDocument" => $oDocument,
                "oUser" => $oUser,
                "aRestrictions" => $aRestrictions,
                "aBamParams" => $aBamParams,
            ]
        ); ?>

    </div>

    <?= $this->template(
        "@Kitt3nPimcoreLayoutsBundle/Resources/views/Includes/Body/Script/lazyload.html.php",
        [
            "oWebsiteConfig" => $oWebsiteConfig,
            "oDocument" => $oDocument,
            "oUser" => $oUser,
            "aRestrictions" => $aRestrictions,
            "aBamParams" => $aBamParams,
        ]
    ); ?>

    <?= $this->template(
        "@Kitt3nPimcoreLayoutsBundle/Resources/views/Includes/Body/Script/navigation.anchor.html.php",
        [
            "oWebsiteConfig" => $oWebsiteConfig,
            "oDocument" => $oDocument,
            "oUser" => $oUser,
            "aRestrictions" => $aRestrictions,
            "aBamParams" => $aBamParams,
        ]
    ); ?>

    <?= $this->template("@Kitt3nPimcoreLayoutsBundle/Resources/views/Includes/Body/overlay.html.php",
        [
            "oWebsiteConfig" => $oWebsiteConfig,
            "oDocument" => $oDocument,
            "oUser" => $oUser,
            "aRestrictions" => $aRestrictions,
            "aBamParams" => $aBamParams,
        ]
    ); ?>
</body>
