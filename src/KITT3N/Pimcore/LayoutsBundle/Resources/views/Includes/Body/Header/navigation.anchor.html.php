<?php
use Symfony\Component\DomCrawler\Crawler;

/** @var \Symfony\Component\Templating\Helper\SlotsHelper $oSlots */
$oSlots = $this->slots();
$sContentSlot = $oSlots->get("_content");

$html = <<<HTML
<!DOCTYPE html>
<html>
    <body>
        {$sContentSlot}
    </body>
</html>
HTML;

$oDom = new Crawler($html);
$oDom = $oDom->filter('body > div.element--anchor > a');

$aDocumentAnchorElements = [];

foreach ($oDom as $domElement) {
    $aDocumentAnchorElements[] = [
        "href" => $domElement->getAttribute("id"),
        "nav_title" => $domElement->textContent,
    ];
}

if (count($aDocumentAnchorElements) > 0) {

    echo "<div class=\"wrapper navigation anchor\">";
    echo "<div class=\"grid-container columns c12\">";
    echo "<div class=\"column\">";
    echo "<nav>";

    foreach ($aDocumentAnchorElements as $key => $aDocumentAnchorElement) {
        echo "<a class=\"anchorLink" . ($key === 0 ? ' active' : '') . "\" href=\"#" . $aDocumentAnchorElement['href'] . "\">" . $aDocumentAnchorElement['nav_title'] . "</a>";
    }

    echo "</nav>";
    echo "</div>";
    echo "</div>";
    echo "</div>";

}

///* @var array $aDocumentElements */
//$aDocumentElements = $oDocument->getElements();
//
//if (! empty($aDocumentElements)) {
//
//    /* @var \Pimcore\Model\Document\Tag\Areablock $oDocumentElementsOverview */
//    $oDocumentElementsOverview = $aDocumentElements["elements"];
//
//    $aDocumentElementsOverviewData = $oDocumentElementsOverview->getData();
//
//    $aDocumentAnchorKeys = [];
//    foreach ($aDocumentElementsOverviewData as $aDocumentElementsOverviewDatum) {
//
//        if ($aDocumentElementsOverviewDatum['type'] === "anchor" and $aDocumentElementsOverviewDatum['hidden'] === false) {
//            $aDocumentAnchorKeys[] = $aDocumentElementsOverviewDatum['key'];
//        }
//
//    }
//
//    /*
//     * DB structure example with two anchors on page 3
//     *
//     * +===============+===============================+===============+===================+
//     * | documentId    | name                          | type          | data              |
//     * +===============+===============================+===============+===================+
//     * | 3             | elements                      | areablock     | a:14:{i:0;a:3...} |
//     * +---------------+-------------------------------+---------------+-------------------+
//     * | 3             | elements:15.anchor_href       | input         | icons             |
//     * +---------------+-------------------------------+---------------+-------------------+
//     * | 3             | elements:15.anchor_nav_title  | input         | Icons             |
//     * +---------------+-------------------------------+---------------+-------------------+
//     * | 3             | elements:16.anchor_href       | input         | bam               |
//     * +---------------+-------------------------------+---------------+-------------------+
//     * | 3             | elements:16.anchor_nav_title  | input         | Bam!              |
//     * +---------------+-------------------------------+---------------+-------------------+
//     *
//     * Layout/Template contains an areablock with key "elements". This areablock contains all
//     * elements (anchors and columns). So we can get the elements if we know there id (int).
//     * Each key in "$aDocumentAnchorKeys" array contains the id of the corresponding element
//     * (e.g. elements:15.anchor_href for id 15).
//     *
//     * To get all anchor element information (title and href) iterate through
//     * "$aDocumentElementsOverviewDatum" and get the element from "$aDocumentAnchorKeys".
//     * "$aDocumentElementsOverviewDatum['key']" contains the id.
//     */
//
//    /* @var $aDocumentAnchorElements */
//    $aDocumentAnchorElements = [];
//    if (count($aDocumentAnchorKeys) >  0) {
//        foreach ($aDocumentAnchorKeys as $aDocumentAnchorKey) {
//
//            if ($aDocumentElements["elements:$aDocumentAnchorKey.anchor_nav_title"] instanceof \Pimcore\Model\Document\Tag\Input) {
//
//                if ($aDocumentElements["elements:$aDocumentAnchorKey.anchor_href"] instanceof \Pimcore\Model\Document\Tag\Input) {
//
//                    $aDocumentAnchorElements[] = [
//                        "nav_title" => $aDocumentElements["elements:$aDocumentAnchorKey.anchor_nav_title"]->getData(),
//                        "href" => $aDocumentElements["elements:$aDocumentAnchorKey.anchor_href"]->getData(),
//                    ];
//
//                }
//
//            }
//
//        }
//    }
//
//    if (count($aDocumentAnchorElements) > 0) {
//
//        echo "<div class=\"wrapper navigation anchor\">";
//        echo "<div class=\"grid-container columns c12\">";
//        echo "<div class=\"column\">";
//        echo "<nav>";
//
//        foreach ($aDocumentAnchorElements as $key => $aDocumentAnchorElement) {
//            echo "<a class=\"anchorLink" . ($key === 0 ? ' active' : '') . "\" href=\"#" . $aDocumentAnchorElement['href'] . "\">" . $aDocumentAnchorElement['nav_title'] . "</a>";
//        }
//
//        echo "</nav>";
//        echo "</div>";
//        echo "</div>";
//        echo "</div>";
//
//    }
//
//}

?>