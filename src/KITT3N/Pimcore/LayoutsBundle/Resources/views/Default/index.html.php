<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 * @var \Pimcore\Model\Document\Page $document
 *
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config         $aBamParams ["oWebsiteConfig"]
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config       $aBamParams ["oWebsiteConfig"]
 */

use Pimcore\Model\DataObject;
use Pimcore\Model\Document;
use Pimcore\Model\Asset;

$this->extend('Kitt3nPimcoreLayoutsBundle::layout.html.php');

?>

<?= $this->areablock('elements', [
    'allowed' => [
        'anchor',
        'one-column',
        'two-columns',
        'three-columns',
        'four-columns',
        'five-columns',
        'six-columns',
    ],
    'sorting' => [
        'anchor',
        'one-column',
        'two-columns',
        'three-columns',
        'four-columns',
        'five-columns',
        'six-columns',
    ],
    "params" => [
        "one-column" => [
            "forceEditInView" => true,
            "aBamParams" => $aBamParams,
//            "editWidth" => "100%",
//            "editHeight" => "600px"
        ],
        'two-columns' => [
            "forceEditInView" => true,
            "aBamParams" => $aBamParams,
//            "editWidth" => "100%",
//            "editHeight" => "600px"
        ],
        'three-columns' => [
            "forceEditInView" => true,
            "aBamParams" => $aBamParams,
//            "editWidth" => "100%",
//            "editHeight" => "600px"
        ],
        'four-columns' => [
            "forceEditInView" => true,
            "aBamParams" => $aBamParams,
//            "editWidth" => "100%",
//            "editHeight" => "600px"
        ],
        'five-columns' => [
            "forceEditInView" => true,
            "aBamParams" => $aBamParams,
//            "editWidth" => "100%",
//            "editHeight" => "600px"
        ],
        'six-columns' => [
            "forceEditInView" => true,
            "aBamParams" => $aBamParams,
//            "editWidth" => "100%",
//            "editHeight" => "600px"
        ],
    ]
]); ?>

