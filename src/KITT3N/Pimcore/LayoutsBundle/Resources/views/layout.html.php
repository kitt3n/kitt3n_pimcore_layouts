<!DOCTYPE html>

<?php

/**
 * @var array $aBamParams
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config         $aBamParams ["oWebsiteConfig"]
 * @var \Pimcore\Model\DataObject\User $aBamParams ["oUser"]
 * @var \Pimcore\Model\Document\Page $aBamParams ["oDocument"]
 * @var \Pimcore\Config\Config       $aBamParams ["oWebsiteConfig"]
 */

use Pimcore\Config;

/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 * @var \Pimcore\Model\Document\Page $document
 */

/** @var \Pimcore\Model\DataObject\User $oUser */
$oUser = $this->oUser;

/* @var \Pimcore\Model\Document\Page $oDocument */
$oDocument = $this->oDocument;
$aDocumentProperties = $this->aDocumentProperties;

/* @var \Pimcore\Config\Config $aWebsiteConfig */
$oWebsiteConfig = $this->oWebsiteConfig;

?>

<?php
$aRestrictions = [];
if ($oUser instanceof \AppBundle\Model\DataObject\User): ?>

    <?php
    /* @var array|null $xRestrictions */
    $xRestrictions = $oUser->getGroup()->getAssetRestrictions();
    $aRestrictions = $xRestrictions === null ? [] : $xRestrictions;
    ?>

<?php else: ?>

    <?php
    /* @var \Pimcore\Config\Config $aWebsiteConfig */
    $oWebsiteConfig =  \Pimcore\Config::getWebsiteConfig();

    /* @var \Pimcore\Model\DataObject\Group $oBamGuestGroup */
    $oBamGuestGroup = $oWebsiteConfig->get('bam.restrictions.group.guest');

    if ($oBamGuestGroup === null or ! ($oBamGuestGroup instanceof \Pimcore\Model\DataObject\Group)) {
        // Error in configuration
    } else {
        // Check if current document is blacklisted
        $xRestrictions = $oBamGuestGroup->getAssetRestrictions();
        $aRestrictions = $xRestrictions === null ? [] : $xRestrictions;
    }
    ?>

<?php endif; ?>

<html lang="<?= array_key_exists('language', $aDocumentProperties) ? $aDocumentProperties["language"]->getData() : ''; ?>">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <title><?= $oDocument->getTitle(); ?></title>
    <meta name="description" content="<?= $oDocument->getDescription(); ?>">

    <?= $this->template(
        "@Kitt3nPimcoreElementsBundle/Resources/views/Includes/Edit/Head/Script/vue.html.php"
    ); ?>

    <?= $this->template(
        "@Kitt3nPimcoreLayoutsBundle/Resources/views/Includes/Head/Script/functions.html.php"
    ); ?>

    <?= $this->template(
        "@Kitt3nPimcoreLayoutsBundle/Resources/views/Includes/Head/Script/lazyload.html.php"
    ); ?>

    <link rel="stylesheet" type="text/css" href="/bundles/kitt3npimcorelayouts/assets/view/css/view.min.css" />
    <link rel="stylesheet" type="text/css" href="/bundles/kitt3npimcoreelements/assets/view/css/view.min.css" />

    <?php if($this->editmode): ?>
        <link rel="stylesheet" type="text/css" href="/bundles/kitt3npimcorelayouts/assets/edit/css/edit.min.css" />
        <link rel="stylesheet" type="text/css" href="/bundles/kitt3npimcoreelements/assets/edit/css/edit.min.css" />
    <?php endif; ?>

</head>

    <?= $this->template(
        "@Kitt3nPimcoreLayoutsBundle/Resources/views/Includes/Body/index.html.php",
        [
            "oWebsiteConfig" => $oWebsiteConfig,
            "oDocument" => $oDocument,
            "oUser" => $oUser,
            "aBamParams" => $aBamParams,
        ]
    ); ?>

</html>