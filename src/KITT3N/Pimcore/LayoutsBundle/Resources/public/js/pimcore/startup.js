pimcore.registerNS("pimcore.plugin.Kitt3nPimcoreLayoutsBundle");

pimcore.plugin.Kitt3nPimcoreLayoutsBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.Kitt3nPimcoreLayoutsBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("Kitt3nPimcoreLayoutsBundle ready!");
    },

    postOpenDocument: function (params, broker) {
        //alert("Kitt3nPimcoreLayoutsBundle postOpenDocument!");
        //console.log(params);
        //console.warn(params.changeDetectorInitData.data);
        // console.warn(broker);
        // var aComboboxes = document.querySelectorAll('[role="combobox"]');
        // console.warn(aComboboxes);
    }
});

var Kitt3nPimcoreLayoutsBundlePlugin = new pimcore.plugin.Kitt3nPimcoreLayoutsBundle();
